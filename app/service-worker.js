this.addEventListener('install', e => {
  e.waitUntil(
    caches.open("basicswer").then(cache => {
      return cache.addAll([
        '/',
        '/index.html',
        '/index.html?homescreen=1',
        '/?homescreen=1',
        '/css/style.css',
        '/css/bootstrap.min.css',
        '/img/gears-144x144.png',
        '/favicon.ico'
      ])
        .then(() => self.skipWaiting());
    })
  )
});

this.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

this.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});