// Modules
var System = require('systemjs');
var express = require('express');
var app = express();
const $https = require('https');
var path = require('path');
var rootPath = path.normalize(__dirname);


// Port
var nodePort = '5432';
app.set('port', (process.env.PORT || nodePort));


// Routing
app.use(express.static(rootPath + '/app'));

app.get('/', function (req, res) {
    res.sendFile(rootPath + '/index.html');
});

app.listen(app.get('port'));
console.log(' Listening on port: ' + app.get('port'));