'use strict';

var gulp = require('gulp');
var del = require('del');
var swPrecache = require('sw-precache');
var exec = require('child_process').exec;

var DEV_DIR = 'client';
var DIST_DIR = 'dist';

gulp.task('generate-service-worker', function (callback) {
    //   var swPrecache = require('sw-precache');
    var path = require('path');
    var rootDir = 'app';
    var config = {
        "cacheId": 'sw_basic_app',
        "navigateFallback": '/shell',
        "dynamicUrlToDependencies":{
            '/':[
                rootDir + '/index.html',
                // rootDir + '/css/*.css',
                // rootDir + '/js/**.js'
            ]
        },
        "staticFileGlobs": [
            rootDir + '/css/*.css',
            rootDir + '/img/*.png',
            rootDir + '/js/**/*.js',
            rootDir + '/*'
            //rootDir + '/**/**.{js,html,css,png,jpg,gif,json,ico}'
            // rootDir + '/**.{html,ico,json}'
        ],
        "stripPrefix": rootDir,
        "verbose": true
    }
    swPrecache.write(path.join(rootDir, 'service-worker.js'), config, callback);
});

gulp.task('serve-dev', ['generate-service-worker-dev'], function () {
    runExpress(3001, DEV_DIR);
});

gulp.task('serve', callback => {
    exec('node index.js', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        // cb(err);
    })
});

gulp.task('default', function () {
    // gulp.run('generate-service-worker');
    gulp.run('serve');
})